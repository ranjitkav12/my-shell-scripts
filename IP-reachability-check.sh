#!/bin/bash

#input file with list of IPs.
input="$(cat $1)"

for i in $input; do
   echo "pinging $i"
   ping -c3 $i > /dev/null
   if [ $? -ne 0 ]
   then
        echo "IP: $i is not reachable"
   else
        echo "Reachable"
   fi
done