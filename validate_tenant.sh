#!/bin/bash                              
##########################################################
#     Name: validate_tenant            Author: mh6522
#     Name: validate_tenant_ra698k`    Author: ra698k
##########################################################
VERSION="1.5"

echo
echo "Tenant Validation (Version: $VERSION)"
echo

#ENVFILE=$1
#NAME=""
#AIC="3"
#TENANT=`basename $1 | cut -f1 -d_`
#echo $TENANT
TOTALRAMNEEDED=327680
TOTALvCPUSNEEDED=22

#--------------------------------------------------------------------
function check_tenant {
   echo "   Checking tenant availability...."

   var=`nova credentials | grep -c $OS_TENANT_NAME`
   if (($var>0))
   then
      echo -e "      \e[31mRequired tenant:      $OS_TENANT_NAME\e[39m is present"
      echo -e "      \e[31mContinue\e[39m"
      
   else
      echo -e "      \e[31mRequired tenant:      $OS_TENANT_NAME\e[39m is not present"
      echo -e "      \e[31mExit\e[39m"
      exit 1
    
   fi

}

#---------------------------------------------------------------------
function check_keypair {
  echo "   Checking Max KEYPAIR ..."

  MAXKEYPAIR=`nova absolute-limits | grep maxTotalKeypairs | cut -f3 -d'|'`

  echo -e "       \e[31mMax KeyPair is $MAXKEYPAIR\e[39m"
}

#---------------------------------------------------------------------
function check_instances {
  echo "   Checking INSTANCE requirements ..."

  INSTANCES_REQUIRED=0
  INSTANCES_TOTAL=0
  INSTANCES_USED=0
  INSTANCES_AVAILABLE=0

  INSTANCES_TOTAL=`nova absolute-limits | grep maxTotalInstances | cut -f3 -d'|' | awk '{ print $1}'`
  INSTANCES_USED=`nova absolute-limits | grep totalInstancesUsed | cut -f3 -d'|' | awk '{ print $1}'`
  INSTANCES_AVAILABLE=`expr $INSTANCES_TOTAL - $INSTANCES_USED`

  INSTANCES_REQUIRED=20

  if (($INSTANCES_REQUIRED<$INSTANCES_AVAILABLE))
  then
    echo -e "       \e[31mInstances check success\e[39m"
  else
    echo -e "       \e[31mInstances check failed\e[39m"
  fi

}

#---------------------------------------------------------------------
function check_cpus {
  echo "   Checking CPU requirements ..."

  CPUS_REQUIRED=0
  CPUS_TOTAL=0
  CPUS_USED=0
  CPUS_AVAILABLE=0

  CPUS_TOTAL=`nova absolute-limits | grep maxTotalCores | cut -f3 -d'|'`
  CPUS_USED=`nova absolute-limits | grep totalCoresUsed | cut -f3 -d'|'`
  CPUS_AVAILABLE=`expr $CPUS_TOTAL - $CPUS_USED`

  #CPUS_REQUIRED=$TOTALVCPUSREQUIRED
  CPUS_REQUIRED=22

  echo "            CPUs Required  - " $CPUS_REQUIRED
  echo "            CPUs Available - " $CPUS_AVAILABLE

  if (($CPUS_REQUIRED<$CPUS_AVAILABLE))
  then
     echo -e "       \e[31mCPU Check success\e[39m"
  else
     echo -e "       \e[31mCPU Check Failed\e[39m"
  fi

  echo 
}

#---------------------------------------------------------------------
function check_ram {
  echo "   Checking RAM requirements ..."

  RAM_REQUIRED=0
  RAM_TOTAL=0
  RAM_USED=0
  RAM_AVAILABLE=0

  RAM_TOTAL=`nova absolute-limits | grep maxTotalRAMSize | cut -f3 -d'|'`
  RAM_USED=`nova absolute-limits | grep totalRAMUsed | cut -f3 -d'|'`
  RAM_AVAILABLE=`expr $RAM_TOTAL - $RAM_USED`

  RAM_TOTAL=`expr $RAM_TOTAL / 1024`
  RAM_USED=`expr $RAM_USED / 1024`
  RAM_AVAILABLE=`expr $RAM_AVAILABLE / 1024`

  RAM_REQUIRED=`expr $TOTALRAMNEEDED / 1024`

  echo "         RAM Required  - " $RAM_REQUIRED gb
  echo "         RAM Available - " $RAM_AVAILABLE gb
  if (($RAM_REQUIRED<$RAM_AVAILABLE))
   then
    echo -e "       \e[31mRAM CHECK Success!\e[39m"
   else
    echo -e "       \e[31mRAM Check Fail\e[39m"
  fi
  echo 
}

#---------------------------------------------------------------------
function check_flavor {
   echo "   Checking flavor requirements ......"

   BGW_FLAVOR=nd.c4r16d20
   SBC_FLAVOR=nd.c2r8d21s8e21

   BGW=0
   SBC=0

   BGW=`nova flavor-list | grep -c nd.c4r16d20` 
   SBC=`nova flavor-list | grep -c nd.c2r8d21s8e21`

   #echo $BGW
   #echo $SBC

   if (($BGW>0))
     then
        echo -e "       \e[31mvBGW Flavor $BGW_FLAVOR available!\e[39m"
     else
        echo -e "       \e[31mvBGW Flavor $BGW_FLAVOR Unavailable!\e[39m"
   fi

   if (($SBC>0))
     then
        echo -e "       \e[31mvSBC Flavor $SBC_FLAVOR available!\e[39m"
     else
        echo -e "       \e[31mvSBC Flavor $SBC_FLAVOR Unavailable!\e[39m"
   fi
   echo
}

#------------------------------------------------------------------------
function check_image {

   echo "   Checking Image requirements ......"

   SBC_IMAGE=USP_SBC-ISC_RHEL7-R36.28.06.0310.x86_64.qcow2
   BGW_IMAGE=USP_SBC-BGW_rhel7.5-3.10.0-862.14.4.C188B07.x86_64.qcow2

   BGW=`nova image-list | grep -c USP_SBC-BGW_rhel7.5-3.10.0-862.14.4.C188B07.x86_64.qcow2` 
   SBC=`nova image-list | grep -c USP_SBC-ISC_RHEL7-R36.28.06.0310.x86_64.qcow2` 

   if (($BGW == 1))
   then
      echo -e "       \e[31mvBGW Image $BGW_IMAGE available!\e[39m"
   else
      echo -e "       \e[31mvBGW Image $BGW_IMAGE Unavailable!\e[39m"
   fi
 
   if (($SBC == 1))
   then
      echo -e "       \e[31mvSBC Image $SBC_IMAGE available!\e[39m"
   else
      echo -e "       \e[31mvSBC Image $SBC_IMAGE Unavailable!\e[39m"
   fi
   echo

}

#--------------------------------------------------------------------
function check_networks {

   echo "   Checking network names ........"
   check_net=1

   input="netlist"

   for i in $(cat $input); do
      var=`nova network-list | grep -c "$i"`
      if (($var==0))
      then 
         check_net=0
         echo  -e "    \e[31mNetwork name: $i not found\e[39m"
      fi
   done

   if ((check_net==1))
   then
     echo  -e "       \e[31mAll required networks exist\e[39m"
   else
     list=`nova network-list | grep vUSP-23804-P-01 | cut -f3 -d'|'`
     if test -z "$list"
     then
         echo
         echo  -e "   \e[34mNo Networks exist in tenant\e[39m"
     else
         echo  -e "   \e[34mThe list of Networks in the tenant are: \e[39m"
         echo $list
     fi
   fi 
   echo
}

#--------------------------------------------------------------------
function check_subnets {

   echo "   Checking subnetwork names ........"
   check_subnet=1

   input="subnetlist"

   for i in $(cat $input); do
      var=`neutron subnet-list | grep -c "$i"`
      if (($var==0))
      then 
         check_subnet=0
         echo  -e "    \e[31msubnet name: $i not found\e[39m"
      fi
   done

   if ((check_subnet==1))
   then
      echo  -e "        \e[31mAll required subnets exist\e[39m"
   else
      list=`neutron subnet-list | grep vUSP-23804-P-01 | cut -f3 -d'|'`
      if test -z "$list"
      then
         echo
         echo  -e "   \e[34mNo subnets exist in tenant\e[39m"
      else
         echo  -e "   \e[34mThe list of subnets in the tenant are: \e[39m"
         echo $list
      fi
   fi
   echo 
}

#--------------------------------------------------------------------

function check_securitygroups {


   echo "   Checking security groups ........"
   check_secgroup=1

   input="seclist"

   for i in $(cat $input); do
      var=`nova secgroup-list | grep -c "$i"`
      if (($var==0))
      then 
         check_secgroup=0
         echo  -e "    \e[31msecurity group: $i not found\e[39m"
      fi
   done

   if ((check_secgroup==1))
   then
      echo  -e "    \e[31mAll required security groups exist\e[39m"
   else
      list=`nova secgroup-list | grep vUSP-23804-P-01 | cut -f3 -d'|'`
      if test -z "$list"
      then
         echo
         echo  -e "   \e[34mNo security Groups exist in tenant\e[39m"
      else
         echo  -e "   \e[34mThe list of security groups in the tenant are: \e[39m"
         echo $list
      fi
   fi
   echo
}

#--------------------------------------------------------------------
function print_arguments {

  echo "    Printing list of arguments ........"
  echo "$1"
  echo "$2"

  for i in $*; do
    echo $i
  done

}


#---------------------------------------------------------------------
#---------------------------- MAIN ROUTINE ---------------------------
if [ "x$OS_USERNAME" == "x" ]
 then
  echo 
  echo "   You must login to Openstack cloud!"
  echo 
  exit
fi


#---------------------------------------------------------------------
#NAME=`basename $1`

#REGION=`echo $NAME | cut -c1-5`
source ~/openstack.sh $REGION

#echo "   NAME: $NAME"
echo "   REGION: $OS_REGION_NAME"
echo "   TENANT: $OS_TENANT_NAME"
echo


#collect_vminfo

#check_keypair
#check_instances
#check_cpus
#check_ram
###check_volumes
###check_storage
#check_flavors
#check_image
#check_ipaddress
#check_security_groups

#rm /tmp/.*.$$
#                                                                                                                                                                                          416,1         Bot
#      CNT=`echo $VMI | cut -f2 -d:`
#      FLAVOR=`grep ${NAME}_flavor_name: $ENVFILE | cut -f2 -d: | sed 's/ //g' | sed "s/'//g"`
#      echo $NAME:$CNT:$FLAVOR >> /tmp/.vminfo.$$
#     done
#  fi
#}


#---------------------------------------------------------------------
#---------------------------- MAIN ROUTINE ---------------------------
if [ "x$OS_USERNAME" == "x" ]
 then
  echo 
  echo "   You must login to Openstack cloud!"
  echo 
  exit
fi


#---------------------------------------------------------------------
#NAME=`basename $1`

#REGION=`echo $NAME | cut -c1-5`
source ~/openstack.sh $REGION

#echo "   NAME: $NAME"
echo "   REGION: $OS_REGION_NAME"
echo "   TENANT: $OS_TENANT_NAME"
echo

check_tenant
check_keypair
check_instances
check_cpus
check_ram
check_flavor
check_image
check_networks
check_subnets
check_securitygroups
#print_arguments
##check_volumes
###check_storage
#check_flavors
#check_image
#check_ipaddress
#check_security_groups

