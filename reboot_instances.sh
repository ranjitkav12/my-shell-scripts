#!/bin/bash                              
##########################################################
#     Name: validate_tenant            Author: mh6522
#     Name: validate_tenant_ra698k`    Author: ra698k
##########################################################
VERSION="1.5"

echo
echo "Tenant Validation (Version: $VERSION)"
echo

#ENVFILE=$1
#NAME=""
#AIC="3"
#TENANT=`basename $1 | cut -f1 -d_`
#echo $TENANT
TOTALRAMNEEDED=327680
TOTALvCPUSNEEDED=22

function reboot_shutoff_vbgw_instances {
    
    echo "Rebooting vbgw instances that are shutoff...."

    rm -rf shutoff_vbgw_vms

    nova list | grep SHUTOFF | grep vbgw | awk '{print $1}' >> shutoff_vbgw_vms

    input="shutoff_vbgw_vms"
    var=`wc -l < $input`

    if [ $var -eq 0 ]
    then
       echo "No vbgw instances are in shutoff state"
       exit
    fi

    for i in $(cat $input); do
        echo "rebooting vbgw instance with ID: $j"
        `nova reboot --hard $j`
    done 

}

function reboot_shutoff_vbgw_instances {
    
    echo "Rebooting vbgw instances that are shutoff...."

    rm -rf shutoff_vbgw_vms

    nova list | grep SHUTOFF | grep vbgw | awk '{print $1}' >> shutoff_vbgw_vms

    input="shutoff_vbgw_vms"
    var=`wc -l < $input`

    if [ $var -eq 0 ]
    then
       echo "No vbgw instances are in shutoff state"
       exit
    fi

    for i in $(cat $input); do
        echo "rebooting vbgw instance with ID: $j"
        `nova reboot --hard $j`
    done 
}

function reboot_error_vbgw_instances {

    echo "Rebooting vbgw instances that are in error...."

    rm -rf error_vsbc_vms

    nova list | grep ERROR | grep vbgw | awk '{print $1}' >> error_vbgw_vms

    input="error_vbgw_vms"
    var=`wc -l < $input`

    if [ $var -eq 0 ]
    then
       echo "No vbgw instances are in error state"
       exit
    fi

    for i in $(cat $input); do
        echo "rebooting vbgw instance with ID: $i"
        `nova reboot --hard $i`
    done

}

function reboot_error_vsbc_instances {

    echo "Rebooting vsbc instances that are in error...."

    rm -rf error_vsbc_vms

    nova list | grep SHUTOFF | grep vsbc | awk '{print $1}' >> error_vsbc_vms

    input="shutoff_vsbc_vms"
    var=`wc -l < $input`

    if [ $var -eq 0 ]
    then
       echo "No vsbc instances are in error state"
       exit
    fi

    for i in $(cat $input); do
        echo "rebooting vsbc instance with ID: $i"
        `nova reboot --hard $i`
    done

}

function shutdown_active_vbgw_instances {

    echo "Shutting vbgw instances that are active...."

    rm -rf active_vbgw_vms

    nova list | grep ACTIVE | grep vbgw | awk '{print $1}' >> active_vbgw_vms

    input="active_vbgw_vms"
    var=`wc -l < $input`

    if [ $var -eq 0 ]
    then
       echo "No vbgw instances are in active state"
       exit
    fi

    for i in $(cat $input); do
        echo "Shutting vbgw instance with ID: $i"
        `nova stop $i`
    done
}

function shutdown_active_vsbc_instances {

    echo "Shutting vsbc instances that are active...."

    rm -rf error_vsbc_vms

    nova list | grep ERROR | grep vsbc | awk '{print $1}' >> error_vsbc_vms

    input="active_vsbc_vms"
    var=`wc -l < $input`

    if [ $var -eq 0 ]
    then
       echo "No vsbc instances are in active state"
       exit
    fi

    for i in $(cat $input); do
        echo "Shutting vsbc instance with ID: $i"
        `nova stop $i`
    done
}

function program_info {


    echo "Before executing this script make sure you"
    echo "want to reboot or shutdown the instances"
    echo "and comment out the function that should not run...."

    echo "1:   reboot shutoff vbgw instances"
    echo "2:   reboot shutoff vsbc instances"
    echo "3:   reboot error   vbgw instances"
    echo "4:   reboot error   vsbc instances"
    echo "5:   shutdown active vbgw instances"
    echo "6:   shutdown active vsbc instances"
    echo "                                   "
    echo "Enter selection:   " 
    read input

    case $input in
      "1")  reboot_shutoff_vbgw_instances;;
      "2")  reboot_shutoff_vsbc_instances;;
      "3")  reboot_error_vbgw_instances;;
      "4")  reboot_error_vsbc_instances;;
      "5")  shutdown_active_vbgw_instances;;
      "6")  shutdown_active_vsbc_instances;;
     *) echo "Invalid option";;
    esac
}


#---------------------------- MAIN ROUTINE ---------------------------
if [ "x$OS_USERNAME" == "x" ]
 then
  echo 
  echo "   You must login to Openstack cloud!"
  echo 
  exit
fi


#---------------------------------------------------------------------
#NAME=`basename $1`

#REGION=`echo $NAME | cut -c1-5`
source ~/openstack.sh $REGION

#echo "   NAME: $NAME"
echo "   REGION: $OS_REGION_NAME"
echo "   TENANT: $OS_TENANT_NAME"
echo

program_info
#reboot_shutoff_vbgw_instances
#reboot_shutoff_vsbc_instances
#reboot_error_vbgw_instances
#reboot_error_vsbc_instances
#shutdown_active_vbgw_instances
#shutdown_active_vbsbc_instances
#print_arguments
##check_volumes
###check_storage
#check_flavors
#check_image
#check_ipaddress
#check_security_groups

