#!/bin/bash

i="4"
while ((i < 78))
do
  echo "pinging 135.63.243.$i"
  ping -c3 135.63.243.$i > /dev/null
  if [ $? -ne 0 ]
  then
     echo "IP: 135.63.243.$i is not reachable"
  else
     echo "Reachable"
  fi
  ((i++))
done